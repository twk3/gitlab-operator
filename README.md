# GitLab Chart Kubernetes Operator

> **Note**: This operator is being used as an introduction to the operator pattern for the team working on the GitLab Chart,
> and may not represent a clean example of the operator pattern. See the [resources](#resources) for better starting points
> if you are looking to build your own operator.

## Summary

This GitLab Operator is responsible for watching for GitLab version changes in the [GitLab Chart](https://gitlab.com/charts/gitlab)
and running the appropriate migration and restart steps require to rollout the
new GitLab version without introducing downtime.

## Operator Components

- [CustomResourceDefinition](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/)
  * Adds a new resource kind/type to the kubernetes extension api
- [API Object Types](https://github.com/kubernetes/community/blob/master/contributors/devel/api-conventions.md#types-kinds)
  * Define the structure of the new resource type
- [Clients and Clientsets](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/api-machinery/client-package-structure.md#high-level-client-sets)
  * Auto-generated, from the `apis/gitlaboperator/types.go`
  * Define the CRUD functions for the new resource type
- Listers
  * Auto-generated, from the `apis/gitlaboperator/types.go`
  * Define list operations for the new resource type
- [Informers](https://github.com/kubernetes/kubernetes/blob/master/staging/src/k8s.io/sample-controller/docs/controller-client-go.md#custom-controller-components)
  * Auto-generated, from the `apis/gitlaboperator/types.go`
  * Provides the event notifications to the controller
- [Controller](https://github.com/kubernetes/community/blob/master/contributors/devel/controllers.md)
  * Same pattern that is used in many core Kubernetes components
  * Registers Handler functions to various CRUD events on instances of the new resource
  * Processes the work queue for applying resources changes to the cluster
- Handler
  * Defines what we want to happen in the cluster for the various resource events

## Compiling from source

In order to compile this project you need [Go 1.9 or newer](https://golang.org/dl), the [deps binary](https://github.com/golang/dep#installation) and [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder) installed.

`make`

## Release and Version information

See the [Release documentation](doc/release.md)

## Resources

- [Introducing the Operator Framework](https://www.redhat.com/en/blog/introducing-operator-framework-building-apps-kubernetes)
- [Custom Resources Concept](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
- [Sample custom controller by trstringer](https://medium.com/@trstringer/create-kubernetes-controllers-for-core-and-custom-resources-62fc35ad64a3)
- [Operator Framework](https://github.com/operator-framework)
  * A group of open source projects meant for creating/managing operators.
- [Kubernetes Custom Controller - Core Resource Handling](https://github.com/trstringer/k8s-controller-core-resource)
  * The initial example this operator was built from
  * An example of a custom Kubernetes controller that's only purpose is to watch for the creation, updating, or deletion of all pods.
- [Kubernetes Sample Controller](https://github.com/kubernetes/sample-controller)
  * A simple controller for watching Foo resources as defined with a CustomResourceDefinition
- [Client Controller Docs](https://github.com/kubernetes/kubernetes/blob/master/staging/src/k8s.io/sample-controller/docs/controller-client-go.md)
- [Kubernetes Deep Dive: Code Generation for CustomResources](https://blog.openshift.com/kubernetes-deep-dive-code-generation-customresources/)
  * Information on generating the ClientSets, Listers, and Informers for your CRD api type
- [Prometheus Operator](https://github.com/coreos/prometheus-operator)
  * A real-world example of an operator
- [K8s Deployment Controller](https://github.com/kubernetes/kubernetes/blob/v1.9.9/pkg/controller/deployment/deployment_controller.go)
  * Kubernetes' native deployment controller, as an example of what can be done with a controller
- [Guide to Kubernetes Operator SDK](https://banzaicloud.com/blog/operator-sdk/)

## Attributions

- The code of gitlab-operator was initially built on the work of github user [@trstringer on sample kubernetes custom controller](https://github.com/trstringer/k8s-controller-core-resource)

- The initial project scaffolding was generated using [kubebuilder](https://github.com/kubernetes-sigs/kubebuilder):
  - `kubebuilder init --domain com --license apache2 --owner "GitLab"`
  - `kubebuilder create api --group gitlab --version v1beta1 --kind GitLab`
